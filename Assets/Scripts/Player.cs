using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using Unity.Netcode.Components;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;

public class Player : NetworkBehaviour
{
    NetworkVariable<float> _Speed = new NetworkVariable<float>(2);
    public NetworkVariable<int> _Score = new NetworkVariable<int>(0);  
    [SerializeField]
    GameObject _Projectile;
    Rigidbody2D _RigidBody2D;
    [SerializeField]
    private GameEvent _PlayerDestroyed;
    private void Awake()
    {
        _RigidBody2D = GetComponent<Rigidbody2D>();
    }
    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();
        if (!IsOwner)
            return;
        Physics2D.IgnoreLayerCollision(6,6);
        ChangePositionRpc(new Vector3(0f, -4f, 0f));
    }
    /*
    private void Test(InputAction.CallbackContext context)
    {
        print((Vector2)transform.position + context.ReadValue<Vector2>() * _Speed.Value);
        MoveCharacterRpc((Vector2)transform.position + context.ReadValue<Vector2>() * _Speed.Value * Time.deltaTime);
    }*/
    public override void OnNetworkDespawn()
    {
        base.OnNetworkDespawn();
    }
    private void Update()
    {
        if (!IsOwner)
            return;
        Vector3 movement = Vector3.zero;
        if (Input.GetKeyDown(KeyCode.P))
            CLienIDAndScoreMessageClientRpc();
        if (Input.GetKeyDown(KeyCode.Space))
            InstanceProjectileRpc(transform.position,Vector3.up * 2f);
        if (Input.GetKey(KeyCode.A))
            movement -= Vector3.right;
        if (Input.GetKey(KeyCode.D))
            movement += Vector3.right;
            
            MoveCharacterPhysicsServerRpc(movement.normalized * _Speed.Value);
    }
    /*
    [Rpc(SendTo.Server)]
    private void MoveCharacterRpc(Vector3 newPosition)
    {
        transform.position = newPosition;
    }*/
    [Rpc(SendTo.Server)]
    private void MoveCharacterPhysicsServerRpc(Vector3 velocity)
    {
        _RigidBody2D.velocity = velocity;
    }
    [Rpc(SendTo.Server)]
    private void ChangePositionRpc(Vector3 playerpos) 
    {
        transform.position= playerpos;
    }
    private void SetBulletID(ulong ownerid,Bullet b, Player p) 
    {
        b.SetPlayerIDRpc(ownerid,p);
    }
    [Rpc(SendTo.Server)]
    private void InstanceProjectileRpc(Vector3 SpawnPosition, Vector3 SpawnDirection) 
    {
        GameObject Projectile = Instantiate(_Projectile);
        NetworkObject InstanceNO = Projectile.GetComponent<NetworkObject>();
        InstanceNO.DestroyWithScene = true;
        InstanceNO.transform.position = SpawnPosition;
        InstanceNO.Spawn();
        InstanceNO.GetComponent<Bullet>().BulletDirRpc(SpawnDirection);
        SetBulletID(OwnerClientId,InstanceNO.GetComponent<Bullet>(),transform.GetComponent<Player>());
    }
    [Rpc(SendTo.Server)]
    public void ChangeScoreRpc(int addscore,ulong id) 
    {
        Debug.Log(id + " "+OwnerClientId);
        if(OwnerClientId == id)
            _Score.Value += addscore;

    }
    [ClientRpc]
    private void CLienIDAndScoreMessageClientRpc(ClientRpcParams clientRpcParams = default) 
    {
        Debug.Log(OwnerClientId);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "EnemyBullet")
        {
            transform.GetComponent<NetworkObject>().Despawn(true);
            _PlayerDestroyed.Raise();
        }
    }
}
