using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;

public class botlimit : NetworkBehaviour
{
    [SerializeField]
    GameEvent _LosseEvent;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Enemy")
        {
            _LosseEvent.Raise();
        }
    }
}
