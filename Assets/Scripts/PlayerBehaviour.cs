using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerBehaviour : NetworkBehaviour
{
    NetworkVariable<float> _Speed = new NetworkVariable<float>(1);
    NetworkVariable<Color> _Color = new NetworkVariable<Color>(Color.white);
    PlayerInput _Input;
    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();
        if (!IsOwner)
            return;
        _Input = GetComponent<PlayerInput>();
        _Input.actions["Movement"].performed += Test;
        _Input.enabled = true;
    }
    private void Test(InputAction.CallbackContext context)
    {
        print((Vector2)transform.position + context.ReadValue<Vector2>() * _Speed.Value);
        MoveCharacterRpc((Vector2)transform.position + context.ReadValue<Vector2>() * _Speed.Value * Time.deltaTime);
    }
    public override void OnNetworkDespawn()
    {
        base.OnNetworkDespawn();
        _Input.actions["Movement"].performed -= Test;
        _Input.enabled = true;
    }

    private void Update()
    {
        /*Vector3 movement = Vector3.zero;

        if (Input.GetKey(KeyCode.W))
            movement += Vector3.up;
        if (Input.GetKey(KeyCode.S))
            movement -= Vector3.up;
        if (Input.GetKey(KeyCode.A))
            movement -= Vector3.right;
        if (Input.GetKey(KeyCode.D))
            movement += Vector3.right;
        if (movement != Vector3.zero)
            MoveCharacterRpc(transform.position + movement.normalized * _Speed.Value * Time.deltaTime);*/
    }
    [Rpc(SendTo.Server)]
    private void MoveCharacterRpc(Vector3 newPosition)
    {
        transform.position = newPosition;
    }
}
