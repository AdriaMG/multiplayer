using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.Netcode;
using Unity.Netcode.Transports.UTP;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;
using static Unity.Netcode.Transports.UTP.UnityTransport;

public class HandleJoinMenu : NetworkBehaviour
{
    [SerializeField]
    private TMP_InputField _InputIP;
    [SerializeField]
    private Button _JoinButton;
    [SerializeField]
    private Button _ExitButton;
    [SerializeField]
    private GameObject _Menu;
    [SerializeField]
    UnityTransport _UTransport;

    void Awake()
    {
        _JoinButton.onClick.AddListener(() =>
        {
            SetIp();
            NetworkManager.Singleton.StartClient();
        });
        _ExitButton.onClick.AddListener(() =>
        {
            gameObject.SetActive(false);
            _Menu.SetActive(true);
        });
    }
    private void SetIp()
    {
        ConnectionAddressData settings = _UTransport.ConnectionData;
        settings.Address = _InputIP.text;
        _UTransport.ConnectionData = settings;
        NetworkManager.NetworkConfig.ConnectionApproval = true;
    }
}
