using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.BossRoom.Infrastructure;
using Unity.Netcode;
using Unity.Netcode.Transports.UTP;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;

public class LobbyManager : NetworkBehaviour
{
    [SerializeField]
    private GameObject _Prefab;
    [SerializeField]
    private List<Color> _ColorList = new List<Color>() {Color.blue, Color.green, Color.gray, Color.white, Color.red, Color.magenta, Color.yellow, Color.cyan};
    [SerializeField]
    private GameObject _StartButton;
    private List<int> _postionsList = new List<int>() {0,1,2,3,4,5,6,7};
    private NetworkVariable<ulong> _IdClientHost = new NetworkVariable<ulong>();
    private DataPlayer DataPlayer;
    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();
        if (IsServer)
        {
            DataPlayer = GameObject.Find("DataPlayer").GetComponent<DataPlayer>();
            /*foreach (LobbyShips Lobs in DataPlayer.MyShips)
            {
                Lobs.nobj.RemoveOwnership();
                Lobs.nobj.Despawn();
            }*/
        }
        //NetworkManager.Singleton.OnClientConnectedCallback += OnClientConnected;
        NetworkManager.Singleton.OnClientDisconnectCallback += OnClientDisConnected;
        OnClientConnectedRpc(NetworkManager.Singleton.LocalClientId);
    }
    /*private void OnClientConnected(ulong clientId)
    {
        if (clientId == NetworkManager.Singleton.LocalClientId)
            OnClientConnectedRpc(clientId);
    }*/
    private void OnClientDisConnected(ulong clientId)
    {
        if (IsServer)
            OnClientDisConnectedRpc(clientId);
    }
    [Rpc(SendTo.Server)]
    private void OnClientConnectedRpc(ulong clientId)
    {
        if (NetworkManager.ConnectedClientsList.Count == 1)
        {
            NetworkManager.NetworkConfig.ConnectionApproval = true;
            _IdClientHost.Value = clientId;
        }
        else if (NetworkManager.ConnectedClientsList.Count >= 2)
        {
            ActiveStartButtonClientRpc(true, new ClientRpcParams
                {
                    Send = new ClientRpcSendParams
                    {
                        TargetClientIds = new ulong[] { _IdClientHost.Value }
                    }
                }
            );
        }

        List<int> poslist = DataPlayer.MyShips.Select(x => x.position).ToList();
        List<int> postoselect = _postionsList.Except(poslist).ToList();
        postoselect.Sort();
        int pos = postoselect[0];

        List<Color> colorlist = DataPlayer.MyShips.Select(x => x.color).ToList();
        List<Color> ColorsToSelect = _ColorList.Except(colorlist).ToList();
        Color col = ColorsToSelect[Random.Range(0, ColorsToSelect.Count)];
        
        NetworkObject nobj = NetworkObjectPool.Singleton.GetNetworkObject(_Prefab, Vector3.zero, Quaternion.identity);
        nobj.Spawn();
        nobj.transform.position = Quaternion.Euler(0, 0, GetComponent<RotationShips>().degrees + 45 * pos) * new Vector3(4, 0, 0);
        nobj.transform.parent = transform;
        nobj.transform.localEulerAngles = new Vector3(0,0, 45 * pos);
        nobj.GetComponent<Renderer>().material.color = col;
        
        LobbyShips Ls = DataPlayer.MyShips.FirstOrDefault(x => x.clientId == clientId);
        if (Ls == null)
        {
            Ls = new LobbyShips(clientId, nobj, pos, col);
            DataPlayer.Add(Ls);
        }
        Ls.nobj = nobj;
        Ls.position = pos;
        //Ls.color = col;

        for (int i = 0; i < transform.childCount; i++)
        {
            ChangeColorClientRpc(i, DataPlayer.MyShips.FirstOrDefault(x => x.nobj.gameObject == transform.GetChild(i).gameObject).color);
        }
    }
    [Rpc(SendTo.ClientsAndHost)]
    private void ChangeColorClientRpc(int pos, Color col)
    {
        transform.GetChild(pos).GetComponent<Renderer>().material.color = col;
    }
    [Rpc(SendTo.Server)]
    private void OnClientDisConnectedRpc(ulong clientId)
    {
        LobbyShips ship;
        ship = DataPlayer.MyShips.FirstOrDefault(x => x.clientId == clientId);
        if (!ship.Equals(null))
        {
            ship.nobj.Despawn();
            DataPlayer.Remove(ship);
            if (NetworkManager.ConnectedClientsList.Count == 2)
            {
                ActiveStartButtonClientRpc(false, new ClientRpcParams
                    {
                        Send = new ClientRpcSendParams
                        {
                            TargetClientIds = new ulong[] { _IdClientHost.Value }
                        }
                    }
                );
            }
        }
        /*if (clientId == _IdClientHost.Value && NetworkManager.ConnectedClientsList.Count > 0)
        {
            List<ulong> clientlist = DataPlayer.MyShips.Select(x => x.clientId).ToList();
            ulong client = clientlist[Random.Range(0, clientlist.Count)];
            SetHostClientRpc( new ClientRpcParams
                {
                    Send = new ClientRpcSendParams
                    {
                        TargetClientIds = new ulong[] { client }
                    }
                }
            );
        }*/
    }
    public override void OnNetworkDespawn()
    {
        base.OnNetworkDespawn();
        //NetworkManager.Singleton.OnClientConnectedCallback -= OnClientConnected;
        NetworkManager.Singleton.OnClientDisconnectCallback -= OnClientDisConnected;
    }
    [ClientRpc]
    private void ActiveStartButtonClientRpc(bool active, ClientRpcParams clientRpcParams = default)
    {
        _StartButton.SetActive(active);
    }
    /*[ClientRpc]
    private void SetHostClientRpc(ClientRpcParams clientRpcParams = default)
    {
        NetworkManager.StartHost();
    }*/
}
