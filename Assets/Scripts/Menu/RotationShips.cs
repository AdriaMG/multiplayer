using System.Collections;
using System.Collections.Generic;
using Unity.BossRoom.Infrastructure;
using Unity.Netcode;
using UnityEngine;

public class RotationShips : NetworkBehaviour
{
    [SerializeField]
    private float _RotationSpeed = 7;
    private float _degrees = 0;
    public float degrees => _degrees;
    private void Update()
    {
        if (IsServer)
            RotateLobbyRpc();
    }
    [Rpc(SendTo.Server)]
    private void RotateLobbyRpc()
    {
        transform.Rotate(Vector3.forward, _RotationSpeed * Time.deltaTime);
        _degrees = transform.eulerAngles.z;
    }
}
