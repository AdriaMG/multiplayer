using System.Collections;
using System.Collections.Generic;
using Unity.BossRoom.Infrastructure;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Test : NetworkBehaviour
{
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            NetworkManager.Singleton.SceneManager.LoadScene("Lobby", LoadSceneMode.Single);
        }
    }
}
