using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ButtonsLobby : NetworkBehaviour
{
    [SerializeField]
    private Button _StartButton;
    [SerializeField]

    void Awake()
    {
        _StartButton.onClick.AddListener(() =>
        {
            SceneGameRpc("Game");
        });
    }
    [Rpc(SendTo.Server)]
    private void SceneGameRpc(string Scene)
    {
        NetworkManager.Singleton.SceneManager.LoadScene(Scene, LoadSceneMode.Single);
    }
}
