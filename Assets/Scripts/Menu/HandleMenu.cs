using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class HandleMenu : NetworkBehaviour
{
    [SerializeField]
    private Button _PlayButton;
    [SerializeField]
    private Button _JoinButton;
    [SerializeField]
    private Button _ExitButton;
    [SerializeField]
    private GameObject _JoinMenu;

    void Awake()
    {
        _PlayButton.onClick.AddListener(() =>
        {
            NetworkManager.Singleton.StartHost();
            ChangeSceneRpc("Lobby");
        });
        _JoinButton.onClick.AddListener(() =>
        {
            gameObject.SetActive(false);
            _JoinMenu.SetActive(true);
        });
        _ExitButton.onClick.AddListener(() =>
        {
            Application.Quit();
        });
    }
    [Rpc(SendTo.Server)]
    private void ChangeSceneRpc(string Scene)
    {
        NetworkManager.Singleton.SceneManager.LoadScene(Scene, LoadSceneMode.Single);
    }
}
