using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;

public class Enemy : NetworkBehaviour
{
    [SerializeField]
    private GameObject _Bullet;

    [Rpc(SendTo.Server)]
    public void InstanceProjectileRpc()
    {
        GameObject Projectile = Instantiate(_Bullet);
        NetworkObject InstanceNO = Projectile.GetComponent<NetworkObject>();
        InstanceNO.transform.position = transform.position;
        InstanceNO.Spawn();
        InstanceNO.GetComponent<Rigidbody2D>().velocity = Vector3.down * 3f;
        Destroy(Projectile,4f);
    }
    
}
