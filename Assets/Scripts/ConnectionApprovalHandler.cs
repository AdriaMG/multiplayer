using UnityEngine;
using Unity.Netcode;
using System;
using UnityEngine.SceneManagement;

public class ConnectionApprovalHandler : MonoBehaviour
{
    private NetworkManager m_NetworkManager;
    public delegate void onDesApproval();
    public onDesApproval WarnFullSerever;

    private void Start()
    {
        m_NetworkManager = GetComponent<NetworkManager>();
        if (m_NetworkManager != null)
        {
            m_NetworkManager.OnClientDisconnectCallback += OnClientDisconnectCallback;
            m_NetworkManager.ConnectionApprovalCallback = ApprovalCheck;
        }
    }

    private void ApprovalCheck(NetworkManager.ConnectionApprovalRequest request, NetworkManager.ConnectionApprovalResponse response)
    {
        if (NetworkManager.Singleton.ConnectedClients.Count >= 8 && SceneManager.GetActiveScene().name != "Game")
            response.Approved = false;
        else
            response.Approved = true;
        response.Reason = "Server Full";
    }

    private void OnClientDisconnectCallback(ulong obj)
    {
        if (!m_NetworkManager.IsServer && m_NetworkManager.DisconnectReason != string.Empty)
        {
            WarnFullSerever?.Invoke();
        }
    }
}