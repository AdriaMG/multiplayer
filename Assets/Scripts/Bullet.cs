using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;

public class Bullet : NetworkBehaviour
{
    [SerializeField]
    private float _Speed;
    Rigidbody2D _RigidBody;
    private ulong _BulletOwnerID;
    Player _BulletOwnerNO;
    [SerializeField]
    private GameEvent _DestroyedEnemyEvent;
    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();
        _RigidBody = GetComponent<Rigidbody2D>();
    }

    public void BulletDirRpc(Vector3 dir) 
    {
        _RigidBody.velocity = dir * _Speed;
    }
    public void SetPlayerIDRpc(ulong ownerid,Player p) 
    {
        _BulletOwnerID = ownerid;
        _BulletOwnerNO = p;
        Debug.Log(ownerid + " " + p);
    }
    private void OnBecameInvisible()
    {
        if (!IsServer)
            return;
        if(NetworkObject.isActiveAndEnabled)
            gameObject.GetComponent<NetworkObject>().Despawn(true);
    }
    public override void OnDestroy()
    {
        base.OnDestroy();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(!IsServer)
            return;
        if (collision.tag == "Enemy")
        {
            if (_BulletOwnerNO != null)
            {
                _BulletOwnerNO.ChangeScoreRpc(10, _BulletOwnerID);
            }
            DespawnObjectsByNetworkObjectReferenceRpc(collision.GetComponent<NetworkObject>());
            DespawnObjectsByNetworkObjectReferenceRpc(gameObject.GetComponent<NetworkObject>());
            _DestroyedEnemyEvent.Raise();
        }
    }
    [Rpc(SendTo.Server)]
    private void DespawnObjectsByNetworkObjectReferenceRpc(NetworkObjectReference reference) 
    {
        if (!reference.TryGet(out NetworkObject n))
            return;
        else
            n.Despawn(true);
    }
}
