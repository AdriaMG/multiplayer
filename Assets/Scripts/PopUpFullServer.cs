using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopUpFullServer : MonoBehaviour
{
    [SerializeField]
    private ConnectionApprovalHandler _HandelApproval;
    [SerializeField]
    private GameObject _PopUp;

    void Awake()
    {
        _HandelApproval.WarnFullSerever += Call;
    }
    private void Call()
    {
        StartCoroutine(CoroutinePopUp());
    }
    private IEnumerator CoroutinePopUp()
    {
        _PopUp.SetActive(true);
        yield return new WaitForSeconds(3f);
        _PopUp.SetActive(false);
    }
}
