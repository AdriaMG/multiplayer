using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;

public class LobbyShips
{
    public LobbyShips(ulong clientId, NetworkObject nobj, int position, Color color)
    {
        _clientId = clientId;
        this.nobj = nobj;
        this.position = position;
        this.color = color;
    }
    private ulong _clientId;
    public NetworkObject nobj;
    public int position;
    public Color color;

    public ulong clientId => _clientId;
}
