using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.BossRoom.Infrastructure;
using Unity.Netcode;
using UnityEngine;

public class GameStarted : NetworkBehaviour
{
    private GameObject _DataPlayer;
    [SerializeField]
    private GameObject _Prefab;
    public override void OnNetworkSpawn()
    {
        if (IsServer)
        {
            _DataPlayer = GameObject.Find("DataPlayer");
            AssignColorsandPlayersRpc();
        }
    }
    [Rpc(SendTo.Server)]
    private void AssignColorsandPlayersRpc()
    {
        int i = 0;
        foreach (LobbyShips Ls in _DataPlayer.GetComponent<DataPlayer>().MyShips)
        {
            NetworkObject nobj = NetworkObjectPool.Singleton.GetNetworkObject(_Prefab, Vector3.zero, Quaternion.identity);
            nobj.Spawn();
            nobj.GetComponent<Renderer>().material.color = Ls.color;
            nobj.ChangeOwnership(Ls.clientId);
            nobj.transform.parent = transform;
            Ls.position = i;
            Ls.nobj = nobj;
            ChangeColorClientRpc(i, Ls.color);
            i++;
        }
    }
    [Rpc(SendTo.ClientsAndHost)]
    private void ChangeColorClientRpc(int pos, Color col)
    {
        transform.GetChild(pos).GetComponent<Renderer>().material.color = col;
    }
}
