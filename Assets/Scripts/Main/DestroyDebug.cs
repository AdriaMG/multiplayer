using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyDebug : MonoBehaviour 
{
    void OnDestroy ()
    { 
        Debug.Log($"{name} was just destroyed");
    }
}