using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Unity.Netcode;
using Unity.VisualScripting;
using UnityEngine;

public class DataPlayer : MonoBehaviour
{
    private List<LobbyShips> _MyShips;
    public ReadOnlyCollection<LobbyShips> MyShips => _MyShips.AsReadOnly();
    void Start()
    {
        _MyShips = new List<LobbyShips>();
        DontDestroyOnLoad(gameObject);
    }
    public void Add(LobbyShips s)
    {
        _MyShips.Add(s);
    }
    public void Remove(LobbyShips s)
    {
        _MyShips.Remove(s);
    }
}
