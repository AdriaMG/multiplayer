using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using Unity.Netcode;
using UnityEngine;

public class Hud : NetworkBehaviour
{
    [SerializeField]
    private TextMeshProUGUI _score;
    [Rpc(SendTo.ClientsAndHost)]
    public void changescoreRpc() 
    {
        StartCoroutine(ChangeHudAfterXTime());
    }
    private List<Player> FindPlayers() 
    {
        List<Player> _players= new List<Player>();
        _players = FindObjectsOfType<Player>().ToList<Player>();
        return _players;
    }
    IEnumerator ChangeHudAfterXTime() 
    {
        yield return new WaitForSeconds(0.2f);
        string scoretext = "";
        foreach (Player p in FindPlayers())
        {
            scoretext += "Player: " + (p.OwnerClientId + 1).ToString() + " Score: " + p._Score.Value.ToString() + "\n";
        }
        _score.SetText(scoretext);
    }
}
