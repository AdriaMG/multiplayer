using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : NetworkBehaviour
{
    [SerializeField]
    private GameObject _Enemy;
    private List<GameObject> _AllEnemies;
    private GameObject[,] _GridEnemies;
    [SerializeField]
    private float _EnemySpeed = 2f;
    private Vector3 _MoveDirection = Vector3.right;
    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();
        if (!IsHost || !IsServer)
            return;
        SpawnEnemiesRpc();
        InvokeRepeating("MoveEnemy", 1f,1f);
        InvokeRepeating("ChangeScene", 1f,1f);
    }
    [Rpc(SendTo.Server)]
    private void SpawnEnemiesRpc() 
    {
        if (!IsHost || !IsServer)
            return;
        _AllEnemies = new List<GameObject>(); 
        _GridEnemies = new GameObject[5, 8];
        for (int i = 0; i < 5; i++) 
        {
            Vector3 posi = new Vector3(-(8f/2f), i * 1f, 0f);
            for (int j = 0; j < 8; j++)
            {
                GameObject en = Instantiate(_Enemy); 
                _AllEnemies.Add(en);
                Vector3 p = posi;
                p.x += j+ 0.3f;
                NetworkObject nen = en.GetComponent<NetworkObject>();
                nen.transform.position = p;
                nen.Spawn(); 
                en.transform.SetParent(this.transform);
                _GridEnemies[i, j] = en; 
            }
        }
    }
    private void ChangeEnemyDirectionX()
    {
        if (!IsHost || !IsServer)
            return;
        Vector3 leftborder = Camera.main.ViewportToWorldPoint(Vector3.zero);
        Vector3 rigthborder = Camera.main.ViewportToWorldPoint(Vector3.right);
        foreach (GameObject enemy in _GridEnemies)
        {
            if (enemy != null)
            {
                if (_MoveDirection == Vector3.right && enemy.transform.position.x >= (rigthborder.x - 1.0f))
                {
                    _MoveDirection.x *= -1f; 
                    transform.position = transform.position + (Vector3.down/2);
                }
                else if (_MoveDirection == Vector3.left && enemy.transform.position.x <= (leftborder.x + 1.0f))
                {
                    _MoveDirection.x *= -1f;
                    transform.position = transform.position + (Vector3.down/2);
                }
            }
        }
    }
    private void MoveEnemy() 
    {
        if (!IsHost || !IsServer)
            return;
        transform.position += _MoveDirection * _EnemySpeed;
        ChangeEnemyDirectionX();
        SpawnEnemyBullet();
    }
    private void SpawnEnemyBullet() 
    {
        for (int i = 0; i < _AllEnemies.Count; i++)
        {
            if (_AllEnemies[i] == null)
            {
                _AllEnemies.Remove(_AllEnemies[i]);
            }
        }
        int randomgo = Random.Range(0,_AllEnemies.Count);
        GameObject g;

        for (int i=0; i < _AllEnemies.Count;i++) 
        {
            if (i == randomgo)
            {
                if(g = _AllEnemies[i])
                    g.GetComponent<Enemy>().InstanceProjectileRpc();
            }
        }
    }
    private int FindPlayers()
    {
        List<Player> _players = new List<Player>();
        _players = FindObjectsOfType<Player>().ToList<Player>();
        Debug.Log(_players.Count);
        return _players.Count;
    }
    public void ChangeScene() 
    {
        if(FindPlayers() == 0 || CountEnemies() == 0)
        {
            ChangeSceneToLobby();
        }
    }
    public void ChangeSceneToLobby() 
    {
        foreach (GameObject enemy in _AllEnemies)
        {
            enemy.GetComponent<NetworkObject>().Despawn(true);
        }
        NetworkManager.SceneManager.LoadScene("Lobby", LoadSceneMode.Single);
    }
    private int CountEnemies() 
    {
        int count = 0;
        foreach (GameObject enemy in _GridEnemies)
        {
            if (enemy != null)
            {
                count++;
            }
        }
        return count;
    }
}
